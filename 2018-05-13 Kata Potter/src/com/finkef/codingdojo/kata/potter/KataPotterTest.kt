package com.finkef.codingdojo.kata.potter

import org.junit.Test
import kotlin.test.assertEquals


class TestKataPotter {

    @Test
    fun calculatePricesTest() {
        var kataPotter =  KataPotter()
        kataPotter.calculatePrices(IntArray(size = 0))

        assertEquals(8.0,kataPotter.calculatePrices(intArrayOf(0)))
        assertEquals(16.0,kataPotter.calculatePrices(intArrayOf(0,0)))
        assertEquals(40.0,kataPotter.calculatePrices(intArrayOf(0,0,0,0,0)))
        assertEquals((8*2*0.95),kataPotter.calculatePrices(intArrayOf(0,1)))
        assertEquals(((8*3*0.90)*2)+(8*1),kataPotter.calculatePrices(intArrayOf(0,0,0,1,1,2,2)))
        assertEquals((8*5*0.75)+(8*0.9*3)+(8*1),kataPotter.calculatePrices(intArrayOf(0,0,0,1,1,2,2,3,4)))
        assertEquals((8*3*0.90*2)+(8*1),kataPotter.calculatePrices(intArrayOf(0,1,2,0,1,2,0)))
        assertEquals(((8*5*0.75)*3)+((8*4*0.8)*2)+(8*7),kataPotter.calculatePrices(intArrayOf(0,1,2,3,4,0,1,2,3,4,0,1,2,3,4,0,1,2,3,0,1,2,3,2,2,2,2,2,2,2)))
    }
}