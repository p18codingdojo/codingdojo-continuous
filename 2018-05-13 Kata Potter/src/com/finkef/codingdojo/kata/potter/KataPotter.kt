package com.finkef.codingdojo.kata.potter

class KataPotter {

    fun calculatePrices(order: IntArray): Double {
        var bookOrder: MutableMap<Int, Int> = mutableMapOf()
        var totalPrice = 0.0

        for (book in order) {
            bookOrder.set(book, bookOrder.getOrDefault(book, 0).plus(1))

        }

        var largestSeriesOfBooksInOrder = countLargestSeriesOfBooksInOrder(bookOrder)
        while (largestSeriesOfBooksInOrder > 1) {
            totalPrice += (8 * countLargestSeriesOfBooksInOrder(bookOrder) * determineDiscountMultiplicator(countLargestSeriesOfBooksInOrder(bookOrder)))
            bookOrder = removeLargestSeriesOfBooksFromOrder(bookOrder)
            largestSeriesOfBooksInOrder = countLargestSeriesOfBooksInOrder(bookOrder)
        }

        totalPrice += (8.0 * (booksInOrder(bookOrder)))

        return totalPrice
    }

    private fun countLargestSeriesOfBooksInOrder(order: MutableMap<Int, Int>): Int {
        var distinctBooks: Int = 0
        for (value in order.values) {
            if (value > 0) distinctBooks = distinctBooks.plus(1)
        }
        return distinctBooks
    }

    private fun removeLargestSeriesOfBooksFromOrder(orderMap: MutableMap<Int, Int>): MutableMap<Int, Int> {
        var modifiedOrderMap: MutableMap<Int, Int> = mutableMapOf()
        var distinctBooks: Int = 0
        for (entry in orderMap) {
            if (entry.value > 0) {
                modifiedOrderMap.put(entry.key, entry.value - 1)
            }
        }
        return modifiedOrderMap
    }

    private fun booksInOrder(order: MutableMap<Int, Int>): Int {
        var totalBooks: Int = 0
        for (value in order.values) {
            totalBooks = totalBooks + value
        }
        return totalBooks
    }

    private fun determineDiscountMultiplicator(distinctBooks: Int): Double {
        var discountMultiplicator: Double = 1.0
        when (distinctBooks) {
            1 -> discountMultiplicator = 1.0
            2 -> discountMultiplicator = 0.95
            3 -> discountMultiplicator = 0.9
            4 -> discountMultiplicator = 0.8
            5 -> discountMultiplicator = 0.75
        }
        return discountMultiplicator
    }
}