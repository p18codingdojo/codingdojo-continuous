import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

//ffinke: See https://blog.philipphauer.de/best-practices-unit-testing-kotlin/
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FizzBuzzTest {
    private val fizzBuzz = FizzBuzz()

    @Test
    @Tag("dojo")
    fun `1 returns "1" `() {
        Assertions.assertEquals("1", fizzBuzz.fizzBuzz(1))
    }

    @Test
    @Tag("dojo")
    fun `2 returns "2" `() {
        Assertions.assertEquals("2", fizzBuzz.fizzBuzz(2))
    }

    @Test
    @Tag("dojo")
    fun `3 returns "fizz" `() {
        Assertions.assertEquals("fizz", fizzBuzz.fizzBuzz(3))
    }

    @Test
    @Tag("dojo")
    fun `9 returns "fizz" `() {
        Assertions.assertEquals("fizz", fizzBuzz.fizzBuzz(9))
    }

    @Test
    @Tag("dojo")
    fun `12 returns "fizz" `() {
        Assertions.assertEquals("fizz", fizzBuzz.fizzBuzz(12))
    }

    @Test
    @Tag("dojo")
    fun `5 returns "buzz" `() {
        Assertions.assertEquals("buzz", fizzBuzz.fizzBuzz(5))
    }

    @Test
    @Tag("dojo")
    fun `20 returns "buzz" `() {
        Assertions.assertEquals("buzz", fizzBuzz.fizzBuzz(20))
    }

    @Test
    @Tag("dojo")
    fun `15 returns "fizzbuzz" `() {
        Assertions.assertEquals("fizzbuzz", fizzBuzz.fizzBuzz(15))
    }

    @Test
    @Tag("dojo")
    fun `30 returns "fizzbuzz" `() {
        Assertions.assertEquals("fizzbuzz", fizzBuzz.fizzBuzz(30))
    }

    @Test
    @Tag("dojo")
    fun `giveMeAll exists" `() {
        //fizzBuzz.giveMeAll()
    }

    @Test
    @Tag("dojo")
    fun `test Performance" `() {
        fizzBuzz.timeTest()
    }

}