package kotlinsamples

import KotlinSamples
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.function.Executable

//ffinke: See https://blog.philipphauer.de/best-practices-unit-testing-kotlin/
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class KotlinSamplesTest {
    private val testString: String
    private val kotlinSamples = KotlinSamples()

    init {
        testString = "test" + "123"
    }

    @Tag("fast")
    @Test
    fun `hell world works`() {

        Assertions.assertAll(
            Executable { Assertions.assertEquals("hi", "h" + "i") },
            Executable { Assertions.assertEquals("abcd", "ab" + "cd") },
            Executable { Assertions.assertEquals("This is hell", kotlinSamples.hellWorld()) }
        )
    }

    @Tag("database")
    @Nested
    inner class ErrorCases {
        @Test
        fun `server sends empty body`() {
        }

        @Test
        fun `server sends invalid json`() {
        }

        @Test
        fun `server sends 500`() {
        }

        @Test
        fun `timeout - server response takes too long`() {
        }

        @Test
        fun `not available at all - wrong url`() {
        }

//        @Test
//        fun `EXAMPLE - this can never work`() {
//            Assertions.assertNotEquals(1.0, 2.0 / 2)
//        }
    }


}