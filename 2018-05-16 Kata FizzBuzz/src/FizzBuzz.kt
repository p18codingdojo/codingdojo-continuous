class FizzBuzz {

    fun fizzBuzz(zahl: Int): String {


//        when (zahl % 15) {
//            0 -> return "fizzbuzz"
//            3, 6, 9, 12 -> return "fizz"
//            5, 10 -> return "buzz"
//        }
//
//        return "$zahl"


//        if ((zahl % 15) == 0) {
//            return "fizzbuzz"
//        }
//        if ((zahl % 3) == 0) {
//            return "fizz"
//        }
//        if ((zahl % 5) == 0) {
//            return "buzz"
//        }
//        return "$zahl"

        var resultString = ""
        if((zahl % 3) == 0){
            resultString = "fizz"
        }
        if((zahl % 5) == 0){
            resultString += "buzz"
        }
        if(resultString == ""){
            return "$zahl"
        }

        return resultString

    }

    fun giveMeAll() {
        (1..100).forEach { println(fizzBuzz(it)) }
    }

    fun timeTest(){
        val date = System.currentTimeMillis()
        (1..100000000).forEach { fizzBuzz(it) }
        println(System.currentTimeMillis() - date)
    }

}