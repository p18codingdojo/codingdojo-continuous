package fizzBuzzDemo

class FizzBuzzDemo {

    fun fizzbuzzList() {
        (1..100).forEach { println(fizzbuzz(it)) }
    }

    fun fizzbuzz(value: Int = 0): String {
        return when {
            value % 15 == 0 -> "fizzbuzz"
            value % 3 == 0 -> "fizz"
            value % 5 == 0 -> "buzz"
            else -> "$value"
        }
    }

    fun timeTest(){
        val date = System.currentTimeMillis()
        (1..100000000).forEach { fizzbuzz(it) }
        println(System.currentTimeMillis() - date)
    }


}