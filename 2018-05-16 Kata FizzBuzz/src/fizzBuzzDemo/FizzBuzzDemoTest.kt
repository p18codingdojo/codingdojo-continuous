package fizzBuzzDemo

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@Tag("demo")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FizzBuzzDemoTest {
    val fizzBuzzDemo = FizzBuzzDemo()
    
    @Test
    fun `fizzbuzz Should Return Fizz If The Number Is Dividable by 3 `() {
        Assertions.assertEquals("fizz", fizzBuzzDemo.fizzbuzz(3))
    }

    @Test
    fun `fizzbuzz Should Return Buzz If The Number Is Dividable By 5 `() {
        Assertions.assertEquals("buzz", fizzBuzzDemo.fizzbuzz(5))
        Assertions.assertEquals("buzz", fizzBuzzDemo.fizzbuzz(10))
    }

    @Test
    fun `fizzbuzz Should Return Buzz If The Number Is Dividable By 15 `() {
        Assertions.assertEquals("fizzbuzz", fizzBuzzDemo.fizzbuzz(15))
        Assertions.assertEquals("fizzbuzz", fizzBuzzDemo.fizzbuzz(30))
    }

    @Test
    fun `fizzbuzz Should Return The Same Number If No Other Requirement Is Fulfilled `() {
        Assertions.assertEquals("1", fizzBuzzDemo.fizzbuzz(1))
        Assertions.assertEquals("2", fizzBuzzDemo.fizzbuzz(2))
        Assertions.assertEquals("4", fizzBuzzDemo.fizzbuzz(4))
    }

    //@Test
    //fun `list call should work`() {
    //    fizzBuzzDemo.fizzbuzzList()
    //}

    @Test
    @Tag("dojo")
    fun `test Performance" `() {
        fizzBuzzDemo.timeTest()
    }
}