class KataPokerHands {

    fun comparePokerhands(pokerHand1: String, pokerHand2: String): PokerResult {
        var pokerResult = PokerResult("White", winningHand = "Blub", reason = "Blub")

        var hand1 = determineHand(convertHandToMap(pokerHand1))
        var hand2 = determineHand(convertHandToMap(pokerHand2))


        return pokerResult
    }

    private fun convertHandToMap(pokerHand: String): MutableMap<Int, String> {
        var pokerHandMap = mutableMapOf<Int, String>()
        pokerHand.split(" ").forEachIndexed({ index, value -> pokerHandMap.put(index, value) })
        return pokerHandMap
    }

    private fun determineHand(pokerhand: MutableMap<Int, String>): HandTypeEnum {

        return HandTypeEnum.FLUSH
    }

    data class PokerResult(var player: String, var reason: String, var winningHand: String)

    enum class HandTypeEnum(val value: Int) {
        HIGH_CARD(0), PAIR(1), TWO_PAIR(2), THREE_OF_A_KIND(3), STRAIGHT(4), FLUSH(5), FULL_HOUSE(6), FOUR_OF_A_KIND(7), STRAIGHT_FLUSH(8)
    }
}