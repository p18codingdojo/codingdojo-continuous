import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import KataPokerHands.PokerResult

@Tag("pokerhands")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class KataPokerHandsTest {
    private val kataPokerHands = KataPokerHands()
    private val pokerHand1 = "2H 3D 5S 9C KD"
    private val pokerHand2 = "2C 3H 4S 8C AH"

    @Test
    fun `comparePokerhands exists `() {
        kataPokerHands.comparePokerhands(pokerHand1, pokerHand2)
    }

    @Test
    fun `comparePokerhands returns PokerResult `() {
        val result = kataPokerHands.comparePokerhands(pokerHand1, pokerHand2)
        Assertions.assertTrue(result is PokerResult)
    }

    @Test
    fun `comparePokerhands returns correct values `(){
        val result = kataPokerHands.comparePokerhands(pokerHand1, pokerHand2)
        Assertions.assertTrue("White".equals(result.player,ignoreCase = true))
        val result2 = kataPokerHands.comparePokerhands(pokerHand2, pokerHand1)
        Assertions.assertTrue("Black".equals(result.player,ignoreCase = true))
    }

}