import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

// https://code.joejag.com/coding-dojo/vending-machine/
// ffinke: See https://blog.philipphauer.de/best-practices-unit-testing-kotlin/
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class VendingMachineTest {
    private val vendingMachine = VendingMachine()

    @Test
    @Tag("dojo")
    fun `use returns empty string `() {
        Assertions.assertTrue(vendingMachine.use("D") is String)
    }

    @Test
    @Tag("dojo")
    fun `use does not return null `() {
        Assertions.assertFalse(vendingMachine.use("D") == null)
    }

    @Test
    @Tag("dojo")
    fun `only coin return returns arrow `() {
        Assertions.assertEquals("->",vendingMachine.use("COIN-RETURN"))
    }

    @Test
    @Tag("dojo")
    fun `COIN, COIN-RETURN returns COIN `() {
        Assertions.assertEquals("-> D",vendingMachine.use("D, COIN-RETURN"))
    }

    @Test
    @Tag("dojo")
    fun `COIN without COIN-RETURN does not return COIN `() {
        Assertions.assertEquals("",vendingMachine.use("D"))
    }

    @Test
    @Tag("dojo")
    fun `COIN, COIN, COIN-RETURN returns COIN, COIN `() {
        Assertions.assertEquals("-> D, N",vendingMachine.use("D, N, COIN-RETURN"))
    }

    @Test
    @Tag("dojo")
    fun `a dollar buys B `() {
        Assertions.assertEquals("-> B",vendingMachine.use("DOLLAR, GET-B"))
    }

    @Test
    @Tag("dojo")
    fun `Q Q D N buys A `() {
        Assertions.assertEquals("-> A",vendingMachine.use("Q, Q, D, N, GET-A"))
    }

    @Test
    @Tag("dojo")
    fun `N DOLLAR buys B `() {
        Assertions.assertEquals("-> B, N",vendingMachine.use("N, DOLLAR, GET-B"))
    }

    @Test
    @Tag("dojo")
    fun `DOLLAR buys A `() {
        Assertions.assertEquals("-> A, Q, D",vendingMachine.use("DOLLAR, GET-A"))
    }
}