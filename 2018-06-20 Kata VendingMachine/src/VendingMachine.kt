import org.jetbrains.annotations.Mutable
import java.util.*

class VendingMachine {
    // https://code.joejag.com/coding-dojo/vending-machine/
    private var change: MutableMap<String, Int>
    private var denominations: MutableMap<String, Int>
    private var itemsAmount: MutableMap<String, Int>
    private var itemsPrice: MutableMap<String, Int>
    private var coinsInserted: List<String>

    init {
        change = mutableMapOf()
        denominations = mutableMapOf("DOLLAR" to 100, "Q" to 25, "D" to 10, "N" to 5)
        itemsAmount = mutableMapOf("A" to 2, "B" to 2, "C" to 2)
        itemsPrice = mutableMapOf("A" to 65, "B" to 100, "C" to 150)
        coinsInserted = listOf()

    }

    fun use(input: String): String {
        var output = ""
        val inputs = input.split(", ")
        val command = inputs.last()
        val buyItem = command.last().toString()
        coinsInserted = inputs.subList(0, inputs.size - 1)


        if (command == "COIN-RETURN") {

            output += "->"
            coinsInserted.forEachIndexed { i, e -> if (i != coinsInserted.size - 1) output += " " + e + "," else output += " " + e }
        } else if (itemsAmount.containsKey(buyItem) && itemsAmount.getOrDefault(buyItem, 0) > 0) {
            val buyItem = command.last().toString()
            output += "-> " + buyItem
            var change = buy(buyItem, coinsInserted)
            change.forEach { e -> output += ", " + e }

            itemsAmount[buyItem]?.minus(1)

        } else if (itemsAmount.containsKey(buyItem) && itemsAmount.getOrDefault(buyItem, 0) == 0) {
            output += "-> buyItem is sold out, sorry"
        }

        return output
    }

    private fun buy(command: String, coins: List<String>): List<String> {
        var moneyValue = 0
        moneyValue = calculateMoneyValue(coins, moneyValue)
        moneyValue -= itemsPrice.getOrDefault(command, 0)
        return computeChange(moneyValue)
    }

    private fun calculateMoneyValue(coins: List<String>, moneyValue: Int): Int {
        var moneyValue1 = moneyValue
        coins.forEach {
            moneyValue1 += denominations.getOrDefault(it, 0)
        }
        return moneyValue1
    }

    private fun computeChange(change: Int): List<String> {
        var mutableChange = change.toInt()
        var changeList = mutableListOf<String>()
        while (mutableChange > 0) {
            denominations.forEach { el ->
                run {
                    if ((mutableChange - el.value) >= 0) {
                        mutableChange -= el.value
                        changeList.add(el.key)
                    }
                }
            }
        }
        return changeList
    }
}