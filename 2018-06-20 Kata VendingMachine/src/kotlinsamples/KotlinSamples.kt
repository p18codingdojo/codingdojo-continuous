import java.awt.Rectangle

class KotlinSamples {

    fun hellWorld(): String {
        return if (imWithStupid()) "This is hell" else "Nopenopenope"
    }

    private fun imWithStupid(): Boolean {
        return true
    }

    private fun parseInt(str: String): Int? {
        return str.toIntOrNull()
    }

    private fun neverCalledProduct(zahl1: String, zahl2: String) {
        val x = parseInt(zahl1)
        val y = parseInt(zahl2)

        println(x?.times(2) ?: "x is null")

        // val z = x * y does not work because kotlin detects this would not be null safe

        if (x != null && y != null) {
            //it works here, because they are automatically casted to non-nullable after they're checked once.
            println(x * y)
        } else {
            println("either $zahl1 or $zahl2 is not a number. maybe both :)")
        }
    }

    private fun neverCalledAutoCastExample(sampleObject: Any): Int? {
        if (sampleObject is String && sampleObject.length > 2) {
            // sampleObject is casted to String inside this if
            return sampleObject.length
        }

        //sampleObject is still of type "Any" (=Object in Java) here

        return null
    }

    private fun `say goodbye to spamming the "new" keyword`() {
        val rechteck = Rectangle(1337, 4711)
    }
}