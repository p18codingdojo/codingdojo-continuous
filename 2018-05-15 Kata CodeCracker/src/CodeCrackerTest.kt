import org.junit.Test
import kotlin.test.assertEquals

class CodeCrackerTest {



    @Test
    fun testDecryptMessage(){
        var codeCracker = CodeCracker()

        codeCracker.decryptMessage("test")

        assertEquals("a",codeCracker.decryptMessage("!"))
        assertEquals("b",codeCracker.decryptMessage(")"))
        assertEquals("abcdefghijklmnopqrstuvwxyz",codeCracker.decryptMessage("!)\"(£*%&><@abcdefghijklmno"))
        assertEquals("abcdefghijklmnopqrstuvwxya",codeCracker.decryptMessage("!)\"(£*%&><@abcdefghijklmn!"))
        assertEquals("flo",codeCracker.decryptMessage("*ad"))
    }

}